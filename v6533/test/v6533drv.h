/*********************************************************************

  Name:         ov6533drv.h
  Created by:   K.Olchanski
                    implementation of the CAENCommLib functions
  Contents:     v6533 64-channel 50 MHz 12-bit ADC

  $Id$
                
*********************************************************************/
#ifndef  OV6533DRV_INCLUDE_H
#define  OV6533DRV_INCLUDE_H

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <CAENComm.h>
#include "v6533.h"

CAENComm_ErrorCode v6533_info(int handle, int *nchannels, uint32_t *data);
CAENComm_ErrorCode v6533_Status(int handle);
CAENComm_ErrorCode v6533_Setup(int handle, int mode);


#endif // OV6533DRV_INCLUDE_H

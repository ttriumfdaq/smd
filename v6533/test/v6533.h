/***************************************************************************/
/*                                                                         */
/*  Filename: V6533.h                                                      */
/*                                                                         */
/*  Function: headerfile for V6533                                         */
/*                                                                         */
/***************************************************************************/

#ifndef  V6533_INCLUDE_H
#define  V6533_INCLUDE_H

#define V6533_VMAX                            0x0050     //!< R A32/D16
#define V6533_IMAX                            0x0054     //!< R A32/D16
#define V6533_STATUS                          0x0058     //!< R A32/D16
#define V6533_FWREL                           0x005C     //!< R A32/D16

#define V6533_VSET                            0x0000     //!< RW A32/D16
#define V6533_ISET                            0x0004     //!< RW A32/D16
#define V6533_VMON                            0x0008     //!< R A32/D16
#define V6533_IMONH                           0x000C     //!< R A32/D16
#define V6533_PW                              0x0010     //!< RW A32/D16
#define V6533_CHSTATUS                        0x0014     //!< R A32/D16
#define V6533_TRIP_TIME                       0x0018     //!< RW A32/D16
#define V6533_SVMAX                           0x001C     //!< RW A32/D16
#define V6533_RAMP_DOWN                       0x0020     //!< RW A32/D16
#define V6533_RAMP_UP                         0x0024     //!< RW A32/D16
#define V6533_PWDOWN                          0x0028     //!< RW A32/D16
#define V6533_POLARITY                        0x0030     //!< R A32/D16
#define V6533_TEMPERATURE                     0x0030     //!< R A32/D16
#define V6533_IMON_RANGE                      0x0034     //!< RW A32/D16
#define V6533_IMONL                           0x0038     //!< R A32/D16

#define V6533_CH0                             0x0080
#define V6533_CH1                             0x0100
#define V6533_CH2                             0x0180
#define V6533_CH3                             0x0200
#define V6533_CH4                             0x0280
#define V6533_CH5                             0x0300

#define V6533_CHNUM                           0x8100     //!< R A32/D16
#define V6533_DESCR                           0x8102     //!< R D16
#define V6533_MODEL                           0x8116     //!< R D16
#define V6533_SERNUM                          0x811E     //!< R D16
#define V6533_VME_FWREL                       0x8120     //!< R D16

uint16_t V6533_CH[]=
  { V6533_CH0, V6533_CH1, V6533_CH2, V6533_CH3, V6533_CH4, V6533_CH5};

#endif  //  V6533_INCLUDE_H



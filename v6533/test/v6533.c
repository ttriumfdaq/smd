/*********************************************************************
  Name:         v6533.c
  Created by:   Pierre-A. Amaudruz / K.Olchanski

  Contents:     V6533 HV 6ch.
 
  $Id$
*********************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "CAENComm.h"
#include "v6533drv.h"

/*****************************************************************/
/*-PAA- For test purpose only */
#ifdef MAIN_ENABLE
int main (int argc, char* argv[]) {

  CAENComm_ErrorCode sCAEN;
  uint16_t reg16;
  int device_handle;
  //  CAEN_DGTZ_ErrorCode zCAEN;
  uint32_t V6533_BASE  = 0x52100000;
  uint16_t i, nch=6;
  
  if (argc>1) {
    sscanf(argv[1],"%x", &V6533_BASE);
  }
  
  // Test under vmic
  sCAEN = CAENComm_OpenDevice(CAENComm_USB, 0, 0, V6533_BASE, &device_handle);
  printf("Open:%d %d\n", device_handle, sCAEN);
  if (sCAEN == CAENComm_Success) {
    
    sCAEN = CAENComm_Read16(device_handle, V6533_FWREL, &reg16);
    printf("FW Rev: 0x%x  (%d)\n", reg16, sCAEN);
    sCAEN = CAENComm_Read16(device_handle, V6533_CHNUM, &nch);
    printf("Ch Num: %d  (%d)\n", nch, sCAEN);

    //    sCAEN = CAENComm_Write16(device_handle, V6533_CH[1]+V6533_PW, 0);
    //    printf("Ch Num: 1 Power=1 (%d)\n", sCAEN);
    
    for (i=0; i<nch; i++) {
      sCAEN = CAENComm_Read16(device_handle, V6533_CH[i]+V6533_TEMPERATURE, &reg16);
      printf("ch[%d] Temp: %d (%d)  ", i, reg16, sCAEN);
      sCAEN = CAENComm_Read16(device_handle, V6533_CH[i]+V6533_PW, &reg16);
      printf("ch[%d] Power: %d (%d)  ", i, reg16, sCAEN);
      sCAEN = CAENComm_Read16(device_handle, V6533_CH[i]+V6533_VMON, &reg16);
      printf("ch[%d] VMON: %d (%d)\n", i, reg16, sCAEN);
      sCAEN = CAENComm_Read16(device_handle, V6533_CH[i]+V6533_RAMP_UP, &reg16);
      printf("ch[%d] RAMPUP: %d (%d)\n", i, reg16, sCAEN);
      sCAEN = CAENComm_Read16(device_handle, V6533_CH[i]+V6533_RAMP_DOWN, &reg16);
      printf("ch[%d] RAMPDOWN: %d (%d)\n", i, reg16, sCAEN);
    }
  }
  sCAEN = CAENComm_CloseDevice(device_handle);
  printf("Close:%d %d\n", device_handle, sCAEN);
  if(sCAEN == CAENComm_Success) {
    device_handle = -1;
  }
  
  return 1;

}
#endif

/* emacs
 * Local Variables:
 * mode:C
 * mode:font-lock
 * tab-width: 8
 * c-basic-offset: 2
 * End:
 */

//end

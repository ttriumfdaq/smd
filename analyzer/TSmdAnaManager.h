#ifndef TSmdAnaManager_hxx_seen
#define TSmdAnaManager_hxx_seen

#include <fstream>
#include <iostream>
#include <vector>

#include "TDppPulseChargeHisto.h"
#include "TDppTimeDiffHisto.h"
#include "TDppStopMuonEng.h"
#include "TDppStopElecEng.h"
#include "TWaveformsHisto.h"
#include "TV1720DppData.h"

using namespace std;
/// Class that holds a couple histogram arrays.
/// Handles filling the histogram arrays.
class TSmdAnaManager {

public:
  //Constructor
  TSmdAnaManager();

  /// Analyze event, fill histograms.
  void AnalyzeEvent(TDataContainer& dataContainer);
  
  // Array of DPP pulse charges
  TDppPulseChargeHisto *DppPulseChargeHisto;
  TDppTimeDiffHisto *DppTimeDiffHisto;
  TDppStopMuonEngHisto *DppStopMuonEngHisto;
  TDppStopElecEngHisto *DppStopElecEngHisto;
  TWaveformsHisto *WaveformsHisto;
 
  //Outfiles contain the relevent data for more complex analysis without incorporating it directly into the Smd_Analyzer
  //The files are saved in the results directory 
  ofstream outfile0C;
  ofstream outfile0T;
  ofstream outfile1C;
  ofstream outfile1T;
  ofstream outfile2C;
  ofstream outfile2T;
  ofstream outfile3C;
  ofstream outfile3T;
  ofstream outfile4C;
  ofstream outfile4T;
  ofstream outfile5C;
  ofstream outfile5T;
  ofstream outfile6C;
  ofstream outfile6T;
  ofstream outfile7C;
  ofstream outfile7T; 
  
  
 private:

  // array of last DPP pulses for each channel
  std::vector<TV1720QT*> lastDppPulses;
  std::vector<TV1720QT*> allDppPulses[8];
};

#endif

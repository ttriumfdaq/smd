#ifndef TDppTimeDiffHisto_hxx_seen
#define TDppTimeDiffHisto_hxx_seen

#include "THistogramArrayBase.h"
#include "TROOT.h"

/// Array of histograms of pulse charge
class TDppTimeDiffHisto : public THistogramArrayBase {
 public:
  TDppTimeDiffHisto(){CreateHistograms();}
  virtual ~TDppTimeDiffHisto(){};
  
  /// No automatic updating; taken care of by SmdAnaManager
  void UpdateHistograms(TDataContainer& dataContainer){};

  void BeginRun(int transition,int run,int time){CreateHistograms();};
  void EndRun(int transition,int run,int time){};

private:

  void CreateHistograms(){  // check if we already have histogramss.
    char tname[100];
    sprintf(tname,"TDppTimeDiffHisto_%i",0);
    
    TH1D *tmp = (TH1D*)gDirectory->Get(tname);
    if (tmp) return;

    // Otherwise make histograms
    clear();
    
    for(int i=0; i<8; i++){// Loop over channel.
      
      char name[100];
      char title[100];
      sprintf(name,"TDppTimeDiffHisto_%i",i);

      sprintf(title,"DPP Pulse Time Difference channel=%i",i);	

      TH1D *tmp = new TH1D(name,title,200,0,20);//If the # of bins is changed SmdDisplay.cxx must also be changed ***
      tmp->SetXTitle("time diff (us)");
      push_back(tmp);
    }
  }

    
};



#endif

#ifndef TV1720DppData_hxx_seen
#define TV1720DppData_hxx_seen

#include <vector>

#include "TGenericData.hxx"


/// Each DPP  pulse has just a module, a channel, a charge and a time.
class TV1720QT {

 public:
  
 TV1720QT(int iTimeTag, int iChannel, int iChargeShort, int iChargeLong,
	  int iBaseline, int iPur, int iLength): 
  TimeTag(iTimeTag),Channel(iChannel),ChargeShort(iChargeShort), ChargeLong(iChargeLong),
    Baseline(iBaseline),Pur(iPur),Length(iLength){};//Not sure what this is?

  int TimeTag;
  int Channel;
  int ChargeShort;
  int ChargeLong;
  int Baseline;
  int Pur;
  int Length;

};


/// Class to store V1720 DPP information
/// for CAEN V1720QT, 250MHz FADC.
///
///
class TV1720DppData: public TGenericData {

public:

  /// Constructor
  TV1720DppData(int bklen, int bktype, const char* name, void *pdata);//?
  
  ~TV1720DppData(){

    for(unsigned int i = 0; i < fQTpulses.size(); i++)
      delete fQTpulses[i];
  }
  
  /// Get Number of pulses in this bank.
  int GetNPulses() const {return fQTpulses.size();};
  
  
  TV1720QT* GetPulse(int i);

  
  void Print();

private:
  
  std::vector<TV1720QT*> fQTpulses;
  

};

#endif

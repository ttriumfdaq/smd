#include "TWaveformsHisto.h"
#include "TV1720WaveformData.h"
#include <fstream>
#include <iostream>


void TWaveformsHisto::UpdateHistograms(TDataContainer& dataContainer){
  
  TV1720WaveformData *waves = dataContainer.GetEventData<TV1720WaveformData>("W200");//
  //The W200 represents the database carrying the individual points of the database
  //I think this function just fills up *waves with the events in the vector TV1720WaveformData
 
 //for(int i = 0; i < 8; i++){
  // GetHistogram(i)->Reset();
  //}

  if(waves){ //What does this mean?
    
    for(int i = 0; i < waves->GetNWaveforms(); i++){
      
      TV1720Waveform *wave = waves->GetWaveform(i); //I can't seem to find the GetWaveform?
      int ch = wave->Channel;
      if (ch>8){
	std::cout<<"Their was an error in wave histo"<<std::endl;
	std::cout<<ch<<std::endl;
      }
      if (ch<8){
	GetHistogram(ch)->Reset();
	for(unsigned int j = 0; j < wave->Samples.size(); j++){
	  GetHistogram(ch)->SetBinContent(j+1,wave->Samples[j]);
      }

    }
    }

  }
  
}


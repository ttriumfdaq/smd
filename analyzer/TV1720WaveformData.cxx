#include "TV1720WaveformData.h"

TV1720WaveformData::TV1720WaveformData(int bklen, int bktype, const char* name, void *pdata):
    TGenericData(bklen, bktype, name, pdata)
{

  int nWaves = GetData32()[0];
  
  int pointer = 2;
  //std::cout << "nwave " << nWaves << std::endl;
  for(int i = 0; i < nWaves; i++){
    
    int ch = ((GetData32()[pointer] & 0xffff0000) >> 16);
    int nsample = (GetData32()[pointer] & 0xffff);
    //std::cout << ch << " " << nsample << std::endl;
    pointer++;
    TV1720Waveform *wave = new TV1720Waveform(ch);
    
    for(int j = 0; j < nsample/2; j++){
      wave->Samples.push_back((GetData32()[pointer] & 0xffff));
      wave->Samples.push_back((GetData32()[pointer] & 0xffff0000) >> 16);
      pointer++;
    }
    
    //fQTpulses.push_back(new TV1720QT(TimeTag,Channel,ChargeShort,
    //				     ChargeLong,Baseline,Pur,Length));

    fWaveforms.push_back(wave);
  }

};


TV1720Waveform* TV1720WaveformData::GetWaveform(int i){

  if(i >= 0 && i < GetNWaveforms())
    return fWaveforms[i];

  return new TV1720Waveform(9999);

}


void TV1720WaveformData::Print(){
  
 
}

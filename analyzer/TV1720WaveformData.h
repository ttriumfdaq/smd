#ifndef TV1720WaveformData_hxx_seen
#define TV1720WaveformData_hxx_seen

#include <vector>

#include "TGenericData.hxx"


/// Each DPP  pulse has just a module, a channel, a charge and a time.
class TV1720Waveform {

 public:
  
 TV1720Waveform( int iChannel):Channel(iChannel){}; //Some sort of Constructor?
  
  int Channel;
  std::vector<unsigned int> Samples;
  
};


/// Class to store V1720 waveforms from DPP processing
/// for CAEN V1720QT, 250MHz FADC.
/// Can use functions from TGenericData
///
class TV1720WaveformData: public TGenericData {

public:

  /// Constructor
  TV1720WaveformData(int bklen, int bktype, const char* name, void *pdata);

  // Deconstructor
  ~TV1720WaveformData(){
    for(unsigned int i = 0; i < fWaveforms.size(); i++)
      delete fWaveforms[i];
  }
  
  /// Get Number of pulses in this bank.
  int GetNWaveforms() const {return fWaveforms.size();};
  
  
  TV1720Waveform* GetWaveform(int i); //?

  
  void Print();

private:
  
  std::vector<TV1720Waveform*> fWaveforms;
  

};

#endif

#include "TSmdAnaManager.h"

#include <fstream>
#include <iostream>

using namespace std;


TSmdAnaManager::TSmdAnaManager(){

  DppPulseChargeHisto = new TDppPulseChargeHisto();
  DppPulseChargeHisto->DisableAutoUpdate();
   
  DppTimeDiffHisto = new TDppTimeDiffHisto();
  DppTimeDiffHisto->DisableAutoUpdate();

  DppStopMuonEngHisto = new TDppStopMuonEngHisto();
  DppStopMuonEngHisto->DisableAutoUpdate();

  DppStopElecEngHisto = new TDppStopElecEngHisto();
  DppStopElecEngHisto->DisableAutoUpdate();
 
  WaveformsHisto = new TWaveformsHisto();
  WaveformsHisto->DisableAutoUpdate();

  
  outfile0C.open("/home/midas/Projects/smd/analyzer/compcoinc/results0C.txt");
  outfile0T.open("/home/midas/Projects/smd/analyzer/compcoinc/results0T.txt");
  outfile1C.open("/home/midas/Projects/smd/analyzer/compcoinc/results1C.txt");
  outfile1T.open("/home/midas/Projects/smd/analyzer/compcoinc/results1T.txt");
  outfile2C.open("/home/midas/Projects/smd/analyzer/compcoinc/results2C.txt");
  outfile2T.open("/home/midas/Projects/smd/analyzer/compcoinc/results2T.txt");
  outfile3C.open("/home/midas/Projects/smd/analyzer/compcoinc/results3C.txt");
  outfile3T.open("/home/midas/Projects/smd/analyzer/compcoinc/results3T.txt");
  outfile4C.open("/home/midas/Projects/smd/analyzer/compcoinc/results4C.txt");
  outfile4T.open("/home/midas/Projects/smd/analyzer/compcoinc/results4T.txt");
  outfile5C.open("/home/midas/Projects/smd/analyzer/compcoinc/results5C.txt");
  outfile5T.open("/home/midas/Projects/smd/analyzer/compcoinc/results5T.txt");
  outfile6C.open("/home/midas/Projects/smd/analyzer/compcoinc/results6C.txt");
  outfile6T.open("/home/midas/Projects/smd/analyzer/compcoinc/results6T.txt");
  outfile7C.open("/home/midas/Projects/smd/analyzer/compcoinc/results7C.txt");
  outfile7T.open("/home/midas/Projects/smd/analyzer/compcoinc/results7T.txt");

 
  for(int i = 0; i < 8; i++)
    lastDppPulses.push_back(0);
  }

void TSmdAnaManager::AnalyzeEvent(TDataContainer& dataContainer){

 
  // Fill the waveform information.
  WaveformsHisto->UpdateHistograms(dataContainer);
 
  // Fill the DPP information
  TV1720DppData *data = dataContainer.GetEventData<TV1720DppData>("DPP0");  
  if(data){
   
    for(int i = 0; i < data->GetNPulses(); i++){
      TV1720QT* pulse = data->GetPulse(i);
      if(0)std::cout << pulse->TimeTag << " " 
		<< pulse->Channel << " " 
		<< pulse->ChargeShort << " " 
		<< pulse->ChargeLong << " " 
		<< pulse->Baseline << " " 
		<< pulse->Pur << std::endl;
      
      
     
      
      DppPulseChargeHisto->GetHistogram(pulse->Channel)->Fill(pulse->ChargeShort);
     
      //Output file section////////////////////////////
      if(pulse->Channel == 0){
	outfile0C << pulse->ChargeShort<<endl;
	outfile0T << pulse->TimeTag<<endl;
       }

      if(pulse->Channel == 1){
	outfile1C << pulse->ChargeShort<<endl;
	outfile1T << pulse->TimeTag<<endl;	
      }
      if (pulse->Channel == 2){
	outfile2C << pulse->ChargeShort<<endl;
	outfile2T << pulse->TimeTag<<endl;	
      }
      if (pulse->Channel == 3){
	outfile3C << pulse->ChargeShort<<endl;
	outfile3T << pulse->TimeTag<<endl;	
      }
      if (pulse->Channel == 4){
	outfile4C << pulse->ChargeShort<<endl;
	outfile4T << pulse->TimeTag<<endl;
      }
      if (pulse->Channel == 5){
	outfile5C << pulse->ChargeShort<<endl;
	outfile5T << pulse->TimeTag<<endl;
      }
      if (pulse->Channel == 6){
	outfile6C << pulse->ChargeShort<<endl;
	outfile6T << pulse->TimeTag<<endl;
      }
      if (pulse->Channel ==7){
	outfile7C << pulse->ChargeShort<< endl;
	outfile7T << pulse->TimeTag<< endl;
      }
      
      /////////////////////////////////////////////////////



      // Calculate time difference compared to last pulse for this channel
      if(lastDppPulses[pulse->Channel]){
	TV1720QT* last_pulse = lastDppPulses[pulse->Channel];
	double tdiff = pulse->TimeTag - last_pulse->TimeTag;//Currently in ns
	// convert from 4 ns bins to microsec
	tdiff *= 4.0/1000.0;
	//Only the stopping muons (i.e. the ones within 10us) are plotted
	if (tdiff < 20){
	  DppTimeDiffHisto->GetHistogram(pulse->Channel)->Fill(tdiff);
	   //Creates the energy spectrum for the Muon and Electron
	   DppStopElecEngHisto->GetHistogram(pulse->Channel)->Fill(pulse->ChargeShort);
	   DppStopMuonEngHisto->GetHistogram(last_pulse->Channel)->Fill(last_pulse->ChargeShort);
	}
      }
    
      // Save this DPP pulse for next time.      
      if(lastDppPulses[pulse->Channel])
	delete lastDppPulses[pulse->Channel];
      lastDppPulses[pulse->Channel] = new TV1720QT(*pulse);
	  
      
    }
  }
 
}
  

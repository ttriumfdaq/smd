#ifndef TWaveformsHisto_hxx_seen
#define TWaveformsHisto_hxx_seen

#include "THistogramArrayBase.h"
#include "TROOT.h"

/// Array of histograms of the V1720 waveforms
class TWaveformsHisto : public THistogramArrayBase {
 public:
  TWaveformsHisto(){CreateHistograms();}
  virtual ~TWaveformsHisto(){};
  

  void UpdateHistograms(TDataContainer& dataContainer);

  void BeginRun(int transition,int run,int time){CreateHistograms();};
  void EndRun(int transition,int run,int time){};

private:

  void CreateHistograms(){  // check if we already have histogramss.
    char tname[100];
    sprintf(tname,"TWaveformsHisto_%i",0);
    
    TH1D *tmp = (TH1D*)gDirectory->Get(tname);
    if (tmp) return;

    // Otherwise make histograms
    clear();
    
    for(int i=0; i<8; i++){// Loop over channel.
      
      char name[100];
      char title[100];
      sprintf(name,"TWaveformsHisto_%i",i);

      sprintf(title,"V1720 Waveform channel=%i",i);	

      TH1D *tmp = new TH1D(name,title,48,-0.5,47.5);
      tmp->SetXTitle("Sample #");
      tmp->SetYTitle("ADC Value");
      push_back(tmp);
    }
  }

    
};



#endif

#include <stdio.h>
#include <iostream>
#include <TH1.h>
#include "TF1.h"
#include <TFile.h>
#include "TRootanaEventLoop.hxx"
#include "TMath.h"
#include "TV1720DppData.h"
#include "TSmdAnaManager.h"
#include "TFancyHistogramCanvas.hxx"
#include "TV1720WaveformData.h"

using namespace std;

class SmdDisplay: public TRootanaEventLoop {
  
  // Analysis manager; analyzes events, fills histograms.
  TSmdAnaManager *anaManager;

public:

  SmdDisplay() {

    anaManager = 0;
  }

  void BeginRun(int transition,int run,int time){
    if(anaManager) delete anaManager;
    anaManager = new TSmdAnaManager();
    
  }

  // Analyze each event
  bool ProcessMidasEvent(TDataContainer& dataContainer){

    anaManager->AnalyzeEvent(dataContainer);
    return true;
  }

  // Write some summary at the end.
  void EndRun(int transition,int run,int time){
  
    ofstream outfile("previous_run_results.txt");

    ofstream resfile;
    resfile.open ("runlog.txt", ios::out | ios::app);
    resfile << run;

    printf("........................................summary\n");
    

    //Fit the time difference histogram and the Pulse Charge Histogram
    double muoncnt [8];
    double passcnt [8];
    double muonerr [8];
    //double passerr [8];
    double error;
    for (int i =0; i<8;i++){
      TF1 *expofit = new TF1("expofit","([0]*[1])*exp(-[1]*x)+[2]",0,20); //Fitting Function
      expofit->SetParName(0,"Constant");
      expofit->SetParName(1,"Rate");
      expofit->SetParName(2,"Background");
      expofit->SetParameter(0,0.1);
      expofit->SetParameter(1,0.1);
      expofit->SetParameter(2,0.1);
      
      outfile << "Channel "<< i <<" Time Difference Fit"<<endl;
      cout << "Channel "<< i <<" Time Difference Fit"<<endl;
      anaManager->DppTimeDiffHisto->GetHistogram(i)->Fit("expofit");
      muoncnt[i] = expofit->GetParameter(0)/0.2;//The 0.1 is hard coded it is the bin width. If the bin width is changed this must be changed as well ***
      error = expofit->GetParError(1)*(1/(expofit->GetParameter(1)*expofit->GetParameter(1)));
      muonerr[i] = expofit->GetParError(0)/0.2;
      outfile<<"The Lifetime of the muon for Channel " << i <<" is "<<1/expofit->GetParameter(1)<<" us "<<"+/- "<<error <<" us" <<endl;
      cout <<"The Lifetime of the muon for Channel " << i <<" is "<<1/expofit->GetParameter(1)<<" us "<<"+/- "<<error <<" us" <<endl;
      outfile<<"The amount of muons detected in PMT " <<i<<" is " <<muoncnt[i]<<" +/- "<< muonerr[i] <<endl;
      cout <<"The amount of muons detected in PMT " <<i<<" is " <<muoncnt[i]<<" +/- "<< muonerr[i] <<endl;
      outfile << "Constant: " <<expofit->GetParameter(0)<<" +/- " <<expofit->GetParError(0)<<endl;
      outfile << "Rate: " <<expofit->GetParameter(1)<<" +/- " <<expofit->GetParError(1)<<endl;
      outfile << "Background: " <<expofit->GetParameter(2)<<" +/- "<<expofit->GetParError(2)<<endl;
      outfile<<"\n"<<endl;
      cout <<"\n"<<endl;
      
      TF1 *g1    = new TF1("g1","gaus",2000,3000);
      outfile << "Channel "<< i <<" Pulse Charge Fit"<<endl;
      cout <<"Channel "<< i <<" Pulse Charge Fit"<<endl;
      anaManager->DppPulseChargeHisto->GetHistogram(i)->Fit(g1,"R");
      passcnt[i] = anaManager->DppPulseChargeHisto->GetHistogram(i)->GetEntries();
      outfile << "The MIP peak for Channel "<< i<<" is " <<g1->GetParameter(1)<<endl;
      cout <<"The MIP peak for Channel "<< i<<" is " <<g1->GetParameter(1)<<endl;
      outfile << "The total passing through muons is " << passcnt[i]<<endl;
      cout <<"The total passing through muons is " << passcnt[i]<<endl;
      outfile <<"Constant: "<< g1->GetParameter(0)<<" +/- "<<g1->GetParError(0)<<endl;
      outfile <<"Mean: "<< g1->GetParameter(1)<<" +/- " <<g1->GetParError(1)<<endl;
      outfile <<"Sigma: "<<g1->GetParameter(2)<<" +/- " <<g1->GetParError(2)<<endl;

      //      resfile << " ch:" << i << " " << g1->GetParameter(1) << "+/-" << g1->GetParameter(2); 
      resfile << "," << i << "," << g1->GetParameter(1) << "," << g1->GetParameter(2); 

      outfile <<"\n"<<endl;
      cout << "\n"<<endl;
    }
     
    
    double passavg1 = (passcnt[0]+passcnt[1])/2;
    double passavg2 = (passcnt[2]+passcnt[3])/2;
    double passavg3 = (passcnt[4]+passcnt[5])/2;
    double passavg4 = (passcnt[6]+passcnt[7])/2;   
    double block1avg = (muoncnt[0]+muoncnt[1])/2;
    double block2avg = (muoncnt[2]+muoncnt[3])/2;
    double block3avg = (muoncnt[4]+muoncnt[5])/2;
    double block4avg = (muoncnt[6]+muoncnt[7])/2;
    double block1err = 0.5*TMath::Power(muonerr[0]*muonerr[0]+muonerr[1]*muonerr[1],0.5);
    double block2err = 0.5*TMath::Power(muonerr[2]*muonerr[2]+muonerr[3]*muonerr[3],0.5);
    double block3err = 0.5*TMath::Power(muonerr[4]*muonerr[4]+muonerr[5]*muonerr[5],0.5);
    double block4err = 0.5*TMath::Power(muonerr[6]*muonerr[6]+muonerr[7]*muonerr[7],0.5);
    double totcnterr = TMath::Power(block1err*block1err+block2err*block2err+block3err*block3err+block4err*block4err,0.5);
    
    outfile << "The average amount of passing through muons for block 1 is "<< passavg1 <<endl;
    outfile << "The average amount of passing through muons for block 2 is "<< passavg2<<endl;
    outfile << "The average amount of passing through muons for block 3 is "<< passavg3<<endl;
    outfile << "The average amount of passing through muons for block 4 is "<< passavg4<<endl;
    outfile << "The average amount of stopping muons for block 1 is " << block1avg <<" +/- "<< block1err << endl;
    outfile << "The average amount of stopping muons for block 2 is " << block2avg <<" +/- "<< block2err << endl;
    outfile << "The average amount of stopping muons for block 3 is " << block3avg <<" +/- "<< block3err << endl;
    outfile << "The average amount of stopping muons for block 4 is " << block4avg <<" +/- "<< block4err << endl;
    outfile << "The total amount of stopping muons are " << block1avg +block2avg+block3avg+block4avg<<" +/- "<<totcnterr <<endl; 
    outfile << "The total amount of passing through muons are " << passavg1+passavg2+passavg3+passavg4<<endl;
    
    cout << " The average amount of passing through muons for block 1 is "<< passavg1 <<endl;
    cout << " The average amount of passing through muons for block 2 is "<< passavg2<<endl;
    cout << " The average amount of passing through muons for block 3 is "<< passavg3<<endl;
    cout << " The average amount of passing through muons for block 4 is "<< passavg4<<endl;
    cout << " The average amount of stopping muons for block 1 is " << block1avg <<" +/- "<< block1err << endl;
    cout << " The average amount of stopping muons for block 2 is " << block2avg <<" +/- "<< block2err << endl;
    cout << " The average amount of stopping muons for block 3 is " << block3avg <<" +/- "<< block3err << endl;
    cout << " The average amount of stopping muons for block 4 is " << block4avg <<" +/- "<< block4err << endl;
    cout << " The total amount of stopping muons are " << block1avg +block2avg+block3avg+block4avg<<" +/- "<<totcnterr <<endl; 
    cout << " The total amount of passing through muons are " << passavg1+passavg2+passavg3+passavg4<<endl;

    //    resfile << " smd: " << block1avg +block2avg+block3avg+block4avg<<" +/- "<<totcnterr <<endl;
    resfile << "," << block1avg+block2avg+block3avg+block4avg<<","<<totcnterr << endl;
    resfile.close();
  }


}; 





int main(int argc, char *argv[])
{
  SmdDisplay::CreateSingleton<SmdDisplay>();  
  return SmdDisplay::Get().ExecuteLoop(argc, argv);
}


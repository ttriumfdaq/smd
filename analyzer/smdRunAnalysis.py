#!/usr/bin/python
# Test python set/get functions

import sys
import os
import optparse
import pythonMidas

# Set Equipment name 
#equip = 'V6533'
#key = 'Equipment/' +  equip  + '/Variables' + '/demand[0]'

#regCurr = pythonMidas.getValue( key )
#print str(regCurr);

if len(sys.argv) == 1:
    # Get run number from ODB
    key = 'Runinfo/run number'
    runN = pythonMidas.getValue( key )
    # Analyze the previous run
    runN = runN-1;
else:
    # Run number passed through arg, use it
    print "arg len = " + str(sys.argv);
    print "Run Number: " + str(sys.argv[1])
    runN = sys.argv[1];

# compose the analysis command line
regCmd = '/home/daquser/online/src/analyzer/SmdDisplay.exe' + ' /home/daquser/data/run00' + str(runN) + '.mid.gz' + ' -s300000000' 
print str(regCmd);
os.system(regCmd);

resultfile = '/home/daquser/online/src/analyzer/previous_run_results.txt'
moveCmd = 'mv ' + resultfile + ' /home/daquser/online/src/analyzer/observations/run_' + str(runN) +'_results.txt'
print str(moveCmd);
os.system(moveCmd);

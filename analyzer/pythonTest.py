#!/usr/bin/python
# Test python set/get functions

import sys
import os
import optparse
import pythonMidas

# Set Equipment name 
equip = 'V6533'
key = 'Equipment/' +  equip  + '/Variables' + '/demand[0]'

if len(sys.argv) == 2:
    print "arg len = " + str(sys.argv);
    print str(sys.argv[1])
else:
    print " 1"

regCurr = pythonMidas.getValue( key )
print str(regCurr);


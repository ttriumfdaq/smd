#ifndef TDppStopMuonEngHisto_hxx_seen
#define TDppStopMuonEngHisto_hxx_seen

#include "THistogramArrayBase.h"
#include "TROOT.h"

/// Array of histograms of pulse charge
class TDppStopMuonEngHisto : public THistogramArrayBase {
 public:
  TDppStopMuonEngHisto(){
    CreateHistograms();
  }
  virtual ~TDppStopMuonEngHisto(){};
  
  /// No automatic updating; taken care of by SmdAnaManager
  void UpdateHistograms(TDataContainer& dataContainer){};
  void BeginRun(int transition,int run,int time){CreateHistograms();};
  void EndRun(int transition,int run,int time){};

private:

  void CreateHistograms(){  // check if we already have histograms.
    char tname[100];
    sprintf(tname,"TDppStopMuonEngHisto_%i",0);
    
    TH1D *tmp = (TH1D*)gDirectory->Get(tname);
    if (tmp) return;

    // Otherwise make histograms
    clear();
    
    for(int i=0; i<8; i++){// Loop over channel.
      
      char name[100]; 
      char title[100];
      sprintf(name,"TDppStopMuonEngHisto_%i",i);

      sprintf(title,"V1720 DPP Stopping Muon Energy channel=%i",i);	

      TH1D *tmp = new TH1D(name,title,100,0,10000);
      tmp->SetXTitle("ADC value");
      push_back(tmp);
    }
  }
 
};



#endif

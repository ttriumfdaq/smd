/*****************************************************************************/
/**
\file v1720CONET2.cxx

## Contents

  This file contains the class implementation for the v1720 module driver.
 *****************************************************************************/

#include <execinfo.h>
#include <sys/time.h>

//
//-----------------------------------------------------------------------------
double GetTime()
{
#if 1
  struct timeval tv;
  gettimeofday(&tv,0);

  return tv.tv_sec + 0.000001 * tv.tv_usec;
#else
  return 0;
#endif
}

#include "v1720CONET2.hxx"

#define UNUSED(x) ((void)(x)) //!< Suppress compiler warnings

using namespace std;

//! Configuration string for this board. (ODB: /Equipment/[eq_name]/Settings/[board_name]/)
const char * v1720CONET2::config_str_board[] = {\
    "setup = INT : 0",\
    "Acq mode = INT : 3",\
    "VME base Address = DWORD : 0x32100000",\
    "Channel Configuration = DWORD : 16",\
    "Buffer organization = INT : 10",\
    "Custom size = INT : 625",\
    "Channel Mask = DWORD : 255",\
    "Trigger Source = DWORD : 1073741824",\
    "Trigger Output = DWORD : 1073741824",\
    "Post Trigger = DWORD : 1000",\
    "almost_full = DWORD : 850",\
    "DPP Record Length = INT : 120",\
    "Aggregate Length = INT : 10",\
    "DPP Acquisition mode = INT : 0",\
    "Save waveforms = INT : 0",\
    "DPP TrigHoldoff = INT : 20",\
    "DPP Threshold = INT[8] :",\
    "[0] 100",					\
    "[1] 100",					\
    "[2] 100",					\
    "[3] 100",					\
    "[4] 100",					\
    "[5] 100",					\
    "[6] 100",					\
    "[7] 100",					\
    "DPP Num Sample Baseline = INT[8] :",	\
    "[0] 4",					\
    "[1] 4",					\
    "[2] 4",					\
    "[3] 4",					\
    "[4] 4",					\
    "[5] 4",					\
    "[6] 4",					\
    "[7] 4",					\
    "DPP Long Gate = INT[8] :",			\
    "[0] 50",					\
    "[1] 50",					\
    "[2] 50",					\
    "[3] 50",					\
    "[4] 50",					\
    "[5] 50",					\
    "[6] 50",					\
    "[7] 50",					\
    "DPP Short Gate = INT[8] :",	  \
    "[0] 10",				  \
    "[1] 10",				  \
    "[2] 10",				  \
    "[3] 10",				  \
    "[4] 10",				  \
    "[5] 10",				  \
    "[6] 10",				  \
    "[7] 10",				  \
    "DPP Pregate Width = INT[8] :",		\
    "[0] 3",					\
    "[1] 3",					\
    "[2] 3",					\
    "[3] 3",					\
    "[4] 3",					\
    "[5] 3",					\
    "[6] 3",					\
    "[7] 3",					\
    "DPP SelfTrig= INT[8] :",	\
    "[0] 1",					\
    "[1] 1",					\
    "[2] 1",					\
    "[3] 1",					\
    "[4] 1",					\
    "[5] 1",					\
    "[6] 1",					\
    "[7] 1",					\
    "DPP Trig Valid Window = INT[8] :",		\
    "[0] 0",					\
    "[1] 0",					\
    "[2] 0",					\
    "[3] 0",					\
    "[4] 0",					\
    "[5] 0",					\
    "[6] 0",					\
    "[7] 0",					\
    "DPP Charge Sensitivity = INT[8] :",	\
    "[0] 0",					\
    "[1] 0",					\
    "[2] 0",					\
    "[3] 0",					\
    "[4] 0",					\
    "[5] 0",					\
    "[6] 0",					\
    "[7] 0",					\
    NULL
};

const char v1720CONET2::history_settings[][NAME_LENGTH] = { "eStored", "busy" };


//
//-----------------------------------------------------------------------------
/**
 * \brief   Constructor for the module object
 *
 * Set the basic hardware parameters
 *
 * \param   [in]  feindex   Frontend index number
 * \param   [in]  link      Optical link number
 * \param   [in]  board     Board number on the optical link
 * \param   [in]  moduleID  Unique ID assigned to module
 */
v1720CONET2::v1720CONET2(int feindex, int link, int board, int moduleID, HNDLE hDB)
: _feindex(feindex), _link(link), _board(board), _moduleID(moduleID), _odb_handle(hDB)
{
  _device_handle = -1;
  _settings_handle = 0;
  verbose = 1;
  _settings_loaded=false;
  _settings_touched=false;
  _running=false;
  mZLE=false;
  mDataType = 0;
 
}

//
//-----------------------------------------------------------------------------
/**
 * Move constructor needed because we're putting v1720CONET2 objects in a vector which requires
 * either the copy or move operation.  The implicit move constructor (or copy constructor)
 * cannot be created by the compiler because our class contains an atomic object with a
 * deleted copy constructor. */
v1720CONET2::v1720CONET2(v1720CONET2&& other) noexcept
: _feindex(std::move(other._feindex)), _link(std::move(other._link)), _board(std::move(other._board)),
  _moduleID(std::move(other._moduleID)), _odb_handle(std::move(other._odb_handle))
{
  _device_handle = std::move(other._device_handle);
  _settings_handle = std::move(other._settings_handle);
  _settings_loaded = std::move(other._settings_loaded);
  _settings_touched = std::move(other._settings_touched);
  _running= std::move(other._running);
}

//
//-----------------------------------------------------------------------------
v1720CONET2& v1720CONET2::operator=(v1720CONET2&& other) noexcept
{
  if (this != &other){  //if trying to assign object to itself

    _feindex = std::move(other._feindex);
    _link = std::move(other._link);
    _board = std::move(other._board);
    _moduleID = std::move(other._moduleID);
    _odb_handle = std::move(other._odb_handle);
    _device_handle = std::move(other._device_handle);
    _settings_handle = std::move(other._settings_handle);
    _settings_loaded = std::move(other._settings_loaded);
    _settings_touched = std::move(other._settings_touched);
    _running= std::move(other._running);
  }

  return *this;
}

//
//-----------------------------------------------------------------------------
/**
 * \brief   Destructor for the module object
 *
 * Nothing to do.
 */
v1720CONET2::~v1720CONET2()
{
}

//
//-----------------------------------------------------------------------------
/**
 * \brief   Get short string identifying the module's index, link and board number
 *
 * \return  name string
 */
string v1720CONET2::GetName()
{
  stringstream txt;
  txt << "Frontend (Idx, Link, Board)" << _feindex << _link << _board;
  return txt.str();
}

//
//-----------------------------------------------------------------------------
/**
 * \brief   Get connected status
 *
 * \return  true if board is connected
 */
bool v1720CONET2::IsConnected()
{
  return (_device_handle >= 0);
}

//
//-----------------------------------------------------------------------------
/**
 * \brief   Get run status
 *
 * \return  true if run is started
 */
bool v1720CONET2::IsRunning()
{
  return _running;
}

//
//-----------------------------------------------------------------------------
/**
 * \brief   Connect the board through the optical link
 *
 * \return  CAENComm Error Code (see CAENComm.h)
 */
v1720CONET2::ConnectErrorCode v1720CONET2::Connect()
{
  memset(&DPPConfig.Params, 0, sizeof(DigitizerParams_t));
  memset(&DPPConfig.DPPParams, 0, sizeof(CAEN_DGTZ_DPP_PSD_Params_t));

#ifdef ACQOPTICLINKTYPE
  // Direct optical connection - for DPP
  DPPConfig.Params.LinkType = CAEN_DGTZ_PCI_OpticalLink;  // Link Type
  DPPConfig.Params.VMEBaseAddress = 0x0;  // For direct CONET connection, VMEBaseAddress must be 0
#endif

#ifdef ACQUSBLINKTYPE
  // Direct optical connection - for DPP
  DPPConfig.Params.LinkType = CAEN_DGTZ_USB;   //CAEN_DGTZ_PCI_OpticalLink;  // Link Type
  DPPConfig.Params.VMEBaseAddress =  0x32100000;  // For direct CONET connection, VMEBaseAddress must be right
#endif

  return Connect(2, 10);  //reasonable default values
}

//
//-----------------------------------------------------------------------------
v1720CONET2::ConnectErrorCode v1720CONET2::Connect(int connAttemptsMax, int secondsBeforeTimeout)
{
  if (verbose) cout << GetName() << "::Connect()\n";
  ConnectErrorCode returnCode;

  if (IsConnected()) {
    cout << "Error: trying to connect already connected board" << endl;
    returnCode = ConnectErrorAlreadyConnected;
  }

  CAEN_DGTZ_ErrorCode zCAEN;
  zCAEN = CAEN_DGTZ_OpenDigitizer( DPPConfig.Params.LinkType, 
				   _link, 
				   _board, 
				   DPPConfig.Params.VMEBaseAddress, 
				   &_device_handle );

  if (zCAEN) {
    _device_handle = -1;
    printf("Connect failed for link:%d board:%d zC:%d\n", _link, _board, zCAEN);
    returnCode = ConnectErrorCaenComm;    
  } else {
    printf("Link#:%d Board#:%d Module_Handle[%d]:%d\n",
	   _link, _board, _moduleID, this->GetDeviceHandle());
    returnCode = ConnectSuccess;
  }

  CAEN_DGTZ_BoardInfo_t           BoardInfo;
  zCAEN = CAEN_DGTZ_GetInfo(_device_handle, &BoardInfo);
  if (zCAEN) {
    printf("<v1720CONET2::Connect> Can't read board info\n");
    return ConnectErrorTimeout;
  }
  printf("\n<v1720CONET2::Connect> Connected to CAEN Digitizer Model %s, recognized as board %d\n", BoardInfo.ModelName, _board);
  printf("<v1720CONET2::Connect> ROC FPGA Release is %s\n", BoardInfo.ROC_FirmwareRel);
  printf("<v1720CONET2::Connect> AMC FPGA Release is %s\n", BoardInfo.AMC_FirmwareRel);
  
  // Check firmware revision (only DPP firmware can be used with this Demo) */                                       
  int MajorNumber;
  sscanf(BoardInfo.AMC_FirmwareRel, "%d", &MajorNumber);
  if (MajorNumber != 131 && MajorNumber != 132 ) {
    printf("<v1720CONET2::Connect> This digitizer doesn't have a DPP-PSD firmware\n");
    return ConnectErrorCaenComm;
  }
  return returnCode;
}

//
//-----------------------------------------------------------------------------
/**
 * \brief   Disconnect the board through the optical link
 *
 * \return  CAENComm Error Code (see CAENComm.h)
 */
bool v1720CONET2::Disconnect()
{
  if (verbose) cout << GetName() << "::Disconnect()\n";

  if (!IsConnected()) {
    cout << "Error: trying to disconnect already disconnected board" << endl;
    return false;
  }
  if (IsRunning()) {
    cout << "Error: trying to disconnect running board" << endl;
    return false;
  }

  if (verbose) cout << "Closing device (i,l,b) = (" << _feindex << "," << _link << "," << _board << ")" << endl;

  CAENComm_ErrorCode sCAEN = CAENComm_ErrorCode(CAEN_DGTZ_CloseDigitizer(_device_handle));

  if(sCAEN == CAENComm_Success){
    _device_handle = -1;
  }
  else
    return false;

  // free memory
  CAEN_DGTZ_FreeReadoutBuffer(&buffer);
  // CAEN_DGTZ_FreeDPPEvents(_device_handle, Events);
  //CAEN_DGTZ_FreeDPPWaveforms(_device_handle, Waveform);

  return true;
}

//
//-----------------------------------------------------------------------------
/**
 * \brief   Start data acquisition
 *
 * Write to Acquisition Control reg to put board in RUN mode. If ODB
 * settings have been changed, re-initialize the board with the new settings.
 * Set _running flag true.
 *
 * \return  CAENComm Error Code (see CAENComm.h)
 */
bool v1720CONET2::StartRun()
{
  if (verbose) cout << GetName() << "::StartRun()\n";
  
  if (IsRunning()) {
    cout << "Error: trying to start already started board" << endl;
    return false;
  }
  
  if (!IsConnected()) {
    cout << "Error: trying to start disconnected board" << endl;
    return false;
  }
  
  /*
  if (_settings_touched) {
    cm_msg(MINFO, "feoV1720", "Note: settings on board %s touched. Re-initializing board.",
	   GetName().c_str());
    cout << "reinitializing" << endl;
  */
  {
    InitializeForAcq();
  }
  
  CAEN_DGTZ_ErrorCode e = CAEN_DGTZ_SWStartAcquisition(_device_handle);
  if ( e== CAEN_DGTZ_Success) {
    printf("<v1720CONET2::StartRun> Acquisition Started for L:%d B:%d\n", _link, _board);
    _running=true;
  } else {
    printf("<v1720CONET2::StartRun> Acquisition Start FAILED for L:%d B:%d Error:%d\n", _link,  _board, e);
    return false;
  }
  return true;
}

//
//-----------------------------------------------------------------------------
/**
 * \brief   Stop data acquisition
 *
 * Write to Acquisition Control reg to put board in STOP mode.
 * Set _running flag false.
 *
 * \return  CAENComm Error Code (see CAENComm.h)
 */
bool v1720CONET2::StopRun()
{
  if (verbose) cout << GetName() << "::StopRun()\n";

  if (!IsRunning()) {
    cout << "Error: trying to stop already stopped board" << endl;
    return false;
  }
  if (!IsConnected()) {
    cout << "Error: trying to stop disconnected board" << endl;
    return false;
  }

  CAEN_DGTZ_ErrorCode e = CAEN_DGTZ_SWStopAcquisition(_device_handle);
  if (e) printf("<v1720CONET2::StopRun> Acquisition Stopped FAILED Error %d\n",  e);
  _running = false;
  return true;
}

//
//-----------------------------------------------------------------------------
/**
 * \brief   Setup board registers using preset (see ov1720.c:ov1720_Setup())
 *
 * Setup board registers using a preset defined in the midas file ov1720.c
 * - Mode 0x0: "Setup Skip\n"
 * - Mode 0x1: "Trigger from FP, 8ch, 1Ks, postTrigger 800\n"
 * - Mode 0x2: "Trigger from LEMO\n"
 *
 * \param   [in]  mode Configuration mode number
 * \return  CAENComm Error Code (see CAENComm.h)
 */
CAENComm_ErrorCode v1720CONET2::SetupPreset(int mode)
{
  return ov1720_Setup(_device_handle, mode);
}
/**
 * \brief   Control data acquisition
 *
 * Write to Acquisition Control reg
 *
 * \param   [in]  operation acquisition mode (see v1720.h)
 * \return  CAENComm Error Code (see CAENComm.h)
 */
CAENComm_ErrorCode v1720CONET2::AcqCtl(uint32_t operation)
{
  uint32_t reg;
  CAEN_DGTZ_ErrorCode zCAEN;
  zCAEN = CAEN_DGTZ_ReadRegister(_device_handle, V1720_ACQUISITION_CONTROL, &reg);

  switch (operation) {
  case V1720_RUN_START:
    zCAEN = CAEN_DGTZ_WriteRegister(_device_handle, V1720_ACQUISITION_CONTROL, (reg | 0x4));
    break;
  case V1720_RUN_STOP:
    zCAEN = CAEN_DGTZ_WriteRegister(_device_handle, V1720_ACQUISITION_CONTROL, (reg & ~( 0x4)));
    break;
  case V1720_REGISTER_RUN_MODE:
    zCAEN = CAEN_DGTZ_WriteRegister(_device_handle, V1720_ACQUISITION_CONTROL, 0x100);
    break;
  case V1720_SIN_RUN_MODE:
    zCAEN = CAEN_DGTZ_WriteRegister(_device_handle, V1720_ACQUISITION_CONTROL, 0x101);
    break;
  case V1720_SIN_GATE_RUN_MODE:
    zCAEN = CAEN_DGTZ_WriteRegister(_device_handle, V1720_ACQUISITION_CONTROL, 0x102);
    break;
  case V1720_MULTI_BOARD_SYNC_MODE:
    zCAEN = CAEN_DGTZ_WriteRegister(_device_handle, V1720_ACQUISITION_CONTROL, 0x103);
    break;
  case V1720_COUNT_ACCEPTED_TRIGGER:
    zCAEN = CAEN_DGTZ_WriteRegister(_device_handle, V1720_ACQUISITION_CONTROL, (reg & ~( 0x8)));
    break;
  case V1720_COUNT_ALL_TRIGGER:
    zCAEN = CAEN_DGTZ_WriteRegister(_device_handle, V1720_ACQUISITION_CONTROL, (reg | 0x8));
    break;
  default:
    printf("operation %d not defined\n", operation);
    break;
  }
  return CAENComm_ErrorCode(zCAEN);
}

//
//-----------------------------------------------------------------------------
/**
 * \brief   Control data acquisition
 *
 * Write to Acquisition Control reg
 *
 * \param   [in]  operation acquisition mode (see v1720.h)
 * \return  CAENComm Error Code (see CAENComm.h)
 */
CAENComm_ErrorCode v1720CONET2::ChannelConfig(uint32_t operation)
{
  return ov1720_ChannelConfig(_device_handle, operation);
}

//
//-----------------------------------------------------------------------------
/**
 * \brief   Read 32-bit register
 *
 * \param   [in]  address  address of the register to read
 * \param   [out] val      value read from register
 * \return  CAENComm Error Code (see CAENComm.h)
 */
CAENComm_ErrorCode v1720CONET2::_ReadReg(DWORD address, DWORD *val)
{
  if (verbose >= 2) cout << GetName() << "::ReadReg(" << hex << address << ")" << endl;
  return CAENComm_ErrorCode(CAEN_DGTZ_ReadRegister(_device_handle, address, val));
}

//
//-----------------------------------------------------------------------------
/**
 * \brief   Write to 32-bit register
 *
 * \param   [in]  address  address of the register to write to
 * \param   [in]  val      value to write to the register
 * \return  CAENComm Error Code (see CAENComm.h)
 */
CAENComm_ErrorCode v1720CONET2::_WriteReg(DWORD address, DWORD val)
{
  if (verbose >= 2) cout << GetName() << "::WriteReg(" << hex << address << "," << val << ")" << endl;
  return CAENComm_ErrorCode(CAEN_DGTZ_WriteRegister(_device_handle, address, val));
}

//
//-----------------------------------------------------------------------------
bool v1720CONET2::ReadReg(DWORD address, DWORD *val)
{
  return (_ReadReg(address, val) == CAENComm_Success);
}

//
//-----------------------------------------------------------------------------
bool v1720CONET2::WriteReg(DWORD address, DWORD val)
{
  return (_WriteReg(address, val) == CAENComm_Success);
}

//
//-----------------------------------------------------------------------------
/**
 * \brief   Poll Event Stored register
 *
 * Check Event Stored register for any event stored
 *
 * \param   [out]  val     Number of events stored
 * \return  CAENComm Error Code (see CAENComm.h)
 */
bool v1720CONET2::Poll(DWORD *val)
{
  CAENComm_ErrorCode sCAEN = CAENComm_ErrorCode(CAEN_DGTZ_ReadRegister(_device_handle, V1720_EVENT_STORED, val));
  return (sCAEN == CAENComm_Success);
}

//
//-----------------------------------------------------------------------------
//! Maximum size of data to read using BLT (32-bit) cycle
#define MAX_BLT_READ_SIZE_BYTES 10000
/**
 * \brief   Read event buffer and create Midas bank
 *
 * Read the event buffer for this module using BLT (32-bit) cycles.
 * This function reads nothing if EVENT_SIZE register was zero.
 *
 * \param   [out]  data         Where to write content of event buffer
 * \param   [out]  dwords_read  Number of DWORDs read from the buffer
 * \return  CAENComm Error Code (see CAENComm.h)
 */
bool v1720CONET2::FillEventBank(char * pevent)
{
  
  // check if board is connected
  if (! this->IsConnected()) {
    cout << "Error: trying to ReadEvent disconnected board" << endl;
    return false;
  }
  
  // grab data stream
  DWORD *pdata = (DWORD *)pevent;
  int ch,ret=0;
  uint32_t ev;
  DPP_Bank_Out_t out;
  uint32_t BufferSize;
  int curev=0;
  char bankName[5];
  DWORD NTotal =0;
  uint32_t NumEvents[MaxNChannels];
  uint32_t reg;

  double time1 = GetTime();

  ret = CAEN_DGTZ_ReadRegister(_device_handle, 0x8158, &reg);
  //  printf("0x8158:%d\n", reg);
  // read in data  (HW -> buffer)
  ret = CAEN_DGTZ_ReadData(_device_handle, CAEN_DGTZ_SLAVE_TERMINATED_READOUT_MBLT, buffer, &BufferSize);
  if (ret) {
    printf("<v1720CONET::FillEventBank> Readout Error\n");
    return false;    
  }
  if ( BufferSize == 0 )
    return true;
  
  double time2 = GetTime();
  
  // read in DPP data  (buffer -> Events)
  ret = CAEN_DGTZ_GetDPPEvents(_device_handle, buffer, BufferSize, (void**)Events, NumEvents);
  if (ret) {
    printf("<v1720CONET2::FillEventBank> Data Error: %d\n", ret);
    return false;
  }
  
  // find the total number of events for this read
  for(ch=0; ch<MaxNChannels; ch++) {
    if (!(DPPConfig.Params.ChannelMask & (1<<ch)))
      continue;
    NTotal+=NumEvents[ch];
  }
  double time3 = GetTime();
  
  // Compose bank for DPP info on all channels
  sprintf(bankName, "DPP%01d", this->_moduleID);
  bk_create(pevent, bankName, TID_DWORD, (void **) &pdata);
  *pdata++ = NTotal;

  for(ch=0; ch<MaxNChannels; ch++) {
    if (!(DPPConfig.Params.ChannelMask & (1<<ch)))
      continue;
    
    // save data to event format
    for(ev=0; ev<NumEvents[ch]; ev++) {
      
      /* Channel */
      out.Channel = ch;
      /* Time Tag */
      out.TimeTag = Events[ch][ev].TimeTag;
      /* Energy */
      out.ChargeShort= Events[ch][ev].ChargeShort;
      out.ChargeLong = Events[ch][ev].ChargeLong;
      out.Baseline =  Events[ch][ev].Baseline;
      out.Pur = Events[ch][ev].Pur;
      
      Event = &Events[ch][ev];
      
      out.Length = 0;
      // save output data to the pointer before saving to the bank
      memcpy( pdata, (void*)&out, sizeof( out ) ); 
      pdata += sizeof(out)/sizeof(DWORD);	  
    }
    // clear waveform for next event
    curev++;	   
  }
  

  // Close bank, includes all the channels
  int bksze = bk_close(pevent, pdata);
  //  printf("DPP bank, BufferSize:%d bksze:%d wf:%d\n", BufferSize, bksze, DPPConfig.Params.SaveWaveforms);
   
  if (DPPConfig.Params.SaveWaveforms) {
    //-------------------------------------------------------------------------
    // Compose bank for WF info on all channels
    sprintf(bankName, "W2%02d", this->_moduleID);
    bk_create(pevent, bankName, TID_DWORD, (void **) &pdata);
    *pdata++ = NTotal;
    *pdata++ = DPPConfig.Params.ChannelMask;
    int cumulsize = 0;
    for(ch=0; ch<MaxNChannels; ch++) {
      if (!(DPPConfig.Params.ChannelMask & (1<<ch)))
	continue;
      
      // save data to event format
      for(ev=0; ev<NumEvents[ch]; ev++) {
	
	// Point to the right event 
	Event = &Events[ch][ev];
	
	// grab the waveform used for DPP calculations
	if(CAEN_DGTZ_DecodeDPPWaveforms(_device_handle, (void**)Event, Waveform)) {
	  printf("Event Error: cannot find DPPWaveforms\n");
	  return -1;
	}
	
	out.Length = Waveform->Ns;
	
	// save waveform to the pointer before saving to the bank
	// copy from the first element of the waveform in memory 
	// to the full length of the array
	cumulsize += out.Length * sizeof( uint16_t );
	//	printf("wf size: %d\n", cumulsize);
	//	if (cumulsize > 400000) continue;
	*pdata++ = ch << 16 | out.Length;
	memcpy( pdata, (void*)&Waveform->Trace1[0], out.Length * sizeof( uint16_t ) ); 
	pdata += out.Length*sizeof( uint16_t )/sizeof(DWORD);
	
      }
      // clear waveform for next event
      curev++;	   
    }

    // Close bank, includes all the channels
    bk_close(pevent,pdata);
    //    printf("W2 raw bank BufferSize:%d bksze:%d\n", BufferSize, bksze);
  }
   
#if 0
  //-------------------------------------------------------------------------
  //
  // Debugging display
  double time4 = GetTime();
  static int ttt = 0;
  ttt++;
  if(ttt%1000==0){
    std::cout << "Num Event : " << NTotal << std::endl;
    std::cout << "Tdiff " 
	      << (time2 - time1)*1000  <<  " " 
	      << (time3 - time2)*1000  <<  " " 
	      << (time4 - time3)*1000  <<  " " 
	      <<std::endl;

    if(1)std::cout << "0x" << std::hex << out.TimeTag << " 0x" << ((unsigned int*)buffer)[0] 
	      << " 0x" << ((unsigned int*)buffer)[1] 
	      << " 0x" << ((unsigned int*)buffer)[2] 
	      << " 0x" << ((unsigned int*)buffer)[3] 
	      << " 0x" << ((unsigned int*)buffer)[4] 
	      << " 0x" << ((unsigned int*)buffer)[5] 
	      << std::dec << std::endl;

    for(int i = 0 ; i < 20; i++){
      std::cout << i << " 0x" << std::hex << ((unsigned int*)buffer)[i] << std::dec << std::endl;
    }
  }
#endif

  return true;
}

//
//-----------------------------------------------------------------------------
/**
 * \brief   Send a software trigger to the board
 *
 * Send a software trigger to the board.  May require
 * software triggers to be enabled in register 0x810C.
 *
 * \return  CAENComm Error Code (see CAENComm.h)
 */
bool v1720CONET2::SendTrigger()
{
  if (verbose) cout << GetName() << "::SendTrigger()" << endl;
  if (!IsConnected()) {
    cout << "Error: trying to SendTrigger disconnected board" << endl;
    return false;
  }

  if (verbose) cout << "Sending Trigger (l,b) = (" << _link << "," << _board << ")" << endl;

  return (WriteReg(V1720_SW_TRIGGER, 0x1) == CAENComm_Success);
}

//
//-----------------------------------------------------------------------------
/**
 * \brief   Set the ODB record for this board
 *
 * Create a record for the board with settings from the configuration
 * string (v1720CONET2::config_str_board) if it doesn't exist or merge with
 * existing record. Create hotlink with callback function for when the
 * record is updated.  Get the handle to the record.
 *
 * Ex: For a frontend with index number 2 and board number 0, this
 * record will be created/merged:
 *
 * /Equipment/FEV1720I2/Settings/Module0
 *
 * \param   [in]  h        main ODB handle
 * \param   [in]  cb_func  Callback function to call when record is updated
 * \return  ODB Error Code (see midas.h)
 */
int v1720CONET2::SetBoardRecord(HNDLE h, void(*cb_func)(INT,INT,void*))
{
  char set_str[500];

  if(_feindex == -1)
    sprintf(set_str, "/Equipment/FEV1720PSD/Settings/Module%d", _moduleID);
  else
    sprintf(set_str, "/Equipment/FEV1720PSD%02d/Settings/Module%d", _feindex, _moduleID);

  if (verbose) cout << GetName() << "::SetBoardRecord(" << h << "," << set_str << ",...)" << endl;
  int status,size;

  //create record if doesn't exist and find key
  status = db_create_record(h, 0, set_str, strcomb(config_str_board));
  if (verbose) cout<<"    "<<set_str<<endl;
  status = db_find_key(h, 0, set_str, &_settings_handle);
  if (status != DB_SUCCESS) cm_msg(MINFO,"FE","Key %s not found", set_str);

  //hotlink
  size = sizeof(V1720_CONFIG_SETTINGS);
  printf("size: %d\n", size);
  status = db_open_record(h, _settings_handle, &config, size, MODE_READ, cb_func, NULL);

  //get actual record
  status = db_get_record(h, _settings_handle, &config, &size, 0);
  printf("get config %d\n",status);
  if (status == DB_SUCCESS) _settings_loaded = true;
  _settings_touched = true;

  return status; //== DB_SUCCESS for success
}

//
//-----------------------------------------------------------------------------
/**
 * \brief   Set the ODB record for history variable names
 *
 * \param   [in]  h        main ODB handle
 * \param   [in]  cb_func  Callback function to call when record is updated
 * \return  ODB Error Code (see midas.h)
 */
int v1720CONET2::SetHistoryRecord(HNDLE h, void(*cb_func)(INT,INT,void*))
{
  char settings_path[200] = "/Equipment/BUFLVL/Settings/";

  if(_feindex == -1)
    sprintf(settings_path, "/Equipment/BUFLVL/Settings/");
  else
    sprintf(settings_path, "/Equipment/BUFLVL%02d/Settings/", _feindex);

  if (verbose) cout << GetName() << "::SetHistoryRecord(" << h << "," << settings_path << ",...)" << endl;
  int status;

  HNDLE settings_key;
  status = db_find_key(h, 0, settings_path, &settings_key);

  if(status == DB_NO_KEY){
    db_create_key(h, 0, settings_path, TID_KEY);
    db_find_key(h, 0, settings_path, &settings_key);
  }

  char tmp[11];
  sprintf(tmp, "Names BL%02d", this->_moduleID);

  char names_path[100];
  strcpy(names_path, settings_path);
  strcat(names_path, tmp);

  db_create_key(h, 0, names_path, TID_STRING);
  HNDLE path_key;
  status = db_find_key(h, 0, names_path, &path_key);

  db_set_data(h, path_key, history_settings, sizeof(history_settings),
      sizeof(history_settings)/NAME_LENGTH, TID_STRING);

  if (status != DB_SUCCESS) cm_msg(MINFO,"SetHistoryRecord","Key %s not found", names_path);
  return status;
}

//
//-----------------------------------------------------------------------------
/**
 * \brief   Initialize the hardware for data acquisition
 *
 * ### Initial setup:
 * - Set FP I/O Ctrl (0x811C) to default settings (output trigger).
 * - Do software reset + clear.
 * - Set up busy daisy chaining
 * - Put acquisition on stop.
 *
 * ### Checks
 * - AMC firmware version (and check that each channel has the same AMC firmware)
 * - ROC firmware version
 * - board type
 *
 * ### Set registers
 * Use a preset if setup != 0 in the config string, or set them manually otherwise.
 *
 * \return  0 on success, -1 on error
 */
int v1720CONET2::InitializeForAcq()
{
  CAEN_DGTZ_ErrorCode ret;
  
  if (verbose) cout << GetName() << "::InitializeForAcq()" << endl;

  if (!_settings_loaded) {
    cout << "Error: cannot call InitializeForAcq() without settings loaded properly" << endl;
    return -1;    }
  if (!IsConnected())     {
    cout << "Error: trying to call InitializeForAcq() to unconnected board" << endl;
    return -1;    }
  if (IsRunning())    {
    cout << "Error: trying to call InitializeForAcq() to already running board" << endl;
    return -1;    }

  cm_msg(MINFO,"feoV1720","### In InitializeForAcq, _settings_loaded: %d, _settings_touched: %d"
	 , _settings_loaded, _settings_touched);

  //don't do anything if settings haven't been changed
  if (_settings_loaded && !_settings_touched)
      return 0;

  /****************************\
  *      DPP parameters        *
  \****************************/
  DPPConfig.Params.IOlev = CAEN_DGTZ_IOLevel_NIM;

  // Direct optical connection
#ifdef ACQOPTICLINKTYPE
  // Direct Optical connection - for DPP
  DPPConfig.Params.LinkType = CAEN_DGTZ_PCI_OpticalLink;  // Link Type
  DPPConfig.Params.VMEBaseAddress = 0x0;  // For direct CONET connection, VMEBaseAddress must be 0
#endif

#ifdef ACQUSBLINKTYPE
  // Direct USB connection - for DPP
  DPPConfig.Params.LinkType = CAEN_DGTZ_USB;   //CAEN_DGTZ_PCI_OpticalLink;  // Link Type
  printf("vme base:0x%x\n", config.vme_base);
  DPPConfig.Params.VMEBaseAddress = config.vme_base; // 0x32100000;  // For direct CONET connection, VMEBaseAddress must be right
#endif
  //DPPConfig.Params.AcqMode = CAEN_DGTZ_DPP_ACQ_MODE_Mixed; // CAEN_DGTZ_DPP_ACQ_MODE_Oscilloscope (0) or 
  //DPPConfig.Params.AcqMode = CAEN_DGTZ_DPP_ACQ_MODE_List;  // CAEN_DGTZ_DPP_ACQ_MODE_List (1) or 
                                                             // CAEN_DGTZ_DPP_ACQ_MODE_Mixed (2)
  DPPConfig.Params.AcqMode = (CAEN_DGTZ_DPP_AcqMode_t) config.DPPacqmode; // CAEN_DGTZ_DPP_ACQ_MODE_List or 
  DPPConfig.Params.RecordLength = config.recordLen;        // Num of samples of the waveforms (only for 
                                                           // Oscilloscope mode)
  DPPConfig.Params.SaveWaveforms = config.savewaveforms;
  DPPConfig.Params.ChannelMask = config.channel_mask;      // Channel enable mask
  DPPConfig.Params.EventAggr = config.AggregateLen;        // 1024 number of events in one aggregate (0=automatic)
  DPPConfig.Params.PulsePolarity = CAEN_DGTZ_PulsePolarityNegative; // Pulse Polarity (this parameter can be 
                                                                    // individual)
  for(int ch=0; ch<8; ch++) {
    DPPConfig.DPPParams.thr[ch] = config.DPPthresh[ch];    // Trigger Threshold
    /* The following parameter is used to specifiy the number of samples for the baseline averaging:
       0 -> absolute Bl
       1 -> 4samp
       2 -> 8samp
       3 -> 16samp
       4 -> 32samp
       5 -> 64samp
       6 -> 128samp */
    DPPConfig.DPPParams.nsbl[ch]  = config.DPPnsbl[ch];      
    DPPConfig.DPPParams.lgate[ch] = config.DPPLongGate[ch];    // Long Gate Width (N*4ns)
    DPPConfig.DPPParams.sgate[ch] = config.DPPShortGate[ch];   // Short Gate Width (N*4ns)
    DPPConfig.DPPParams.pgate[ch] = config.DPPPregateWidth[ch];// Pre Gate Width (N*4ns) also known as gate offset
    /* Self Trigger Mode:
       0 -> Disabled
       1 -> Enabled */
    DPPConfig.DPPParams.selft[ch] = config.DPPSelfTrig[ch];
    // Trigger configuration:
    // CAEN_DGTZ_DPP_TriggerConfig_Peak       -> trigger on peak. NOTE: Only for FW <= 13X.5
    // CAEN_DGTZ_DPP_TriggerConfig_Threshold  -> trigger on threshold */
    DPPConfig.DPPParams.trgc[ch] = CAEN_DGTZ_DPP_TriggerConfig_Threshold;
    /* Trigger Validation Acquisition Window */
    DPPConfig.DPPParams.tvaw[ch] = config.DPPtvw[ch];
    /* Charge sensitivity: 0->40fc/LSB; 1->160fc/LSB; 2->640fc/LSB; 3->2,5pc/LSB */
    DPPConfig.DPPParams.csens[ch] = config.DPPChargeSen[8];
  }
  /* Pile-Up rejection Mode
     CAEN_DGTZ_DPP_PSD_PUR_DetectOnly -> Only Detect Pile-Up
     CAEN_DGTZ_DPP_PSD_PUR_Enabled -> Reject Pile-Up */
  // purity not yet usable
  DPPConfig.DPPParams.purh = CAEN_DGTZ_DPP_PSD_PUR_DetectOnly;
  DPPConfig.DPPParams.purgap = 100;    // Purity Gap
  DPPConfig.DPPParams.blthr = 100;     // Baseline Threshold
  DPPConfig.DPPParams.bltmo = 100;     // Baseline Timeout
  DPPConfig.DPPParams.trgho = config.DPPTrigHoldoff;      // Trigger HoldOff 
  // end setting DPP Parameters
 

  //PAA-
  CAEN_DGTZ_FreeReadoutBuffer(&buffer);
  //  CAEN_DGTZ_FreeDPPEvents(_device_handle, (void**) Events);
  CAEN_DGTZ_FreeDPPWaveforms(_device_handle, Waveform);

  // reset digitizer
  ret = CAEN_DGTZ_Reset(_device_handle);
  ret = CAEN_DGTZ_SetDPPAcquisitionMode(_device_handle, DPPConfig.Params.AcqMode, 
					CAEN_DGTZ_DPP_SAVE_PARAM_EnergyAndTime);
  printf("SetDPPAcquisitionMode: %d (%d)\n", DPPConfig.Params.AcqMode, ret);
  if (ret != CAEN_DGTZ_Success) {
    printf("<v1720CONET2::InitializeForAcq> Error in CAEN_DGTZ_SetDPPAcquisitionMode %d\n",ret);
    printf("%d\n",_device_handle);
    return (int)ret;
  }

  // Set the digitizer acquisition mode (CAEN_DGTZ_SW_CONTROLLED or CAEN_DGTZ_S_IN_CONTROLLED)
  ret = CAEN_DGTZ_SetAcquisitionMode(_device_handle, CAEN_DGTZ_SW_CONTROLLED);
  printf("SetAcquisitionMode: CONTROLLED (%d)\n", ret);
  if(ret) {
    printf("Error setting Acquisition Mods\n");
    return (int)ret;
  }

  // Set the I/O level (CAEN_DGTZ_IOLevel_NIM or CAEN_DGTZ_IOLevel_TTL)
  ret = CAEN_DGTZ_SetIOLevel(_device_handle, DPPConfig.Params.IOlev);
  printf("SetIOLevel: %d (%d)\n", DPPConfig.Params.IOlev, ret);
  if(ret) {
    printf("Error setting IOLevel %d\n",ret);
    return (int)ret;
  }

  // Set trigger mode
  ret = CAEN_DGTZ_SetExtTriggerInputMode(_device_handle, CAEN_DGTZ_TRGMODE_ACQ_ONLY);
  printf("SetExtTriggerInputMode: TRGMODE_ACQ_ONLY (%d)\n",  ret);
  if (ret != CAEN_DGTZ_Success) {
    printf("<v1720CONET2::InitializeForAcq> Error in CAEN_DGTZ_SetExtTriggerInputMode %d\n",ret);
    return (int)ret;
  }
 
  // Set the enabled channels
  ret = CAEN_DGTZ_SetChannelEnableMask(_device_handle, DPPConfig.Params.ChannelMask);  
  printf("SetChannelEnableMask: %d (%d)\n", DPPConfig.Params.ChannelMask, ret);

  // Set how many events to accumulate in the board memory before being available for readout
  ret = CAEN_DGTZ_SetDPPEventAggregation(_device_handle, DPPConfig.Params.EventAggr, 0);
  printf("Aggregate: %d (%d)\n", DPPConfig.Params.EventAggr, ret);
  if (ret != CAEN_DGTZ_Success) {
    printf("<v1720CONET2::InitializeForAcq> Error in CAEN_DGTZ_SetDPPEventAggregation %d\n",ret);
    return (int)ret;
  }
    
  /* Set the mode used to syncronize the acquisition between different boards.
     In this example the sync is disabled */
  ret = CAEN_DGTZ_SetRunSynchronizationMode(_device_handle, CAEN_DGTZ_RUN_SYNC_Disabled);
    
  // Set the DPP specific parameters for the channels in the given channelMask
  ret = CAEN_DGTZ_SetDPPParameters(_device_handle, config.channel_mask, &DPPConfig.DPPParams);
  printf("SetDPPParameters: %d (%d)\n", config.channel_mask, ret);
  if (ret != CAEN_DGTZ_Success) {
    printf("<v1720CONET2::InitializeForAcq> Error in CAEN_DGTZ_SetDPPParameters %d\n",ret);
    return (int)ret;
  }


  int retval=0;
  for(int i=0; i<8; i++) {

    if (DPPConfig.Params.ChannelMask & (1<<i)) {

      // Set the number of samples for each waveform (you can set different RL for different channels)              
      retval |= CAEN_DGTZ_SetRecordLength(_device_handle, DPPConfig.Params.RecordLength, i);
      
      // Set a DC offset to the input signal to adapt it to digitizer's dynamic range                               
      retval |= CAEN_DGTZ_SetChannelDCOffset(_device_handle, i, 0x8000);
      
      // Set the Pre-Trigger size (in samples)                                                                      
      retval |= CAEN_DGTZ_SetDPPPreTriggerSize(_device_handle, i, 10); 
      if(retval)
	printf("<v1720CONET2::InitializeForAcq> Error setting DPPPreTriggerSize %d\n",retval);
      
      // Set the polarity for the given channel (CAEN_DGTZ_PulsePolarityPositive or CAEN_DGTZ_PulsePolarityNegative)
      retval |= CAEN_DGTZ_SetChannelPulsePolarity(_device_handle, i, DPPConfig.Params.PulsePolarity);
      if(retval)
	printf("<v1720CONET2::InitializeForAcq> Error setting ChannelPulsePolarity %d\n",retval);
      
    }
  }

  // Set the virtual probes
  ret = CAEN_DGTZ_SetDPP_PSD_VirtualProbe(_device_handle, CAEN_DGTZ_DPP_VIRTUALPROBE_SINGLE, 
					  CAEN_DGTZ_DPP_PSD_VIRTUALPROBE_Baseline, 
					  CAEN_DGTZ_DPP_PSD_DIGITALPROBE1_R6_ExtTrg,
					  CAEN_DGTZ_DPP_PSD_DIGITALPROBE2_R6_OverThr);
  if (ret != CAEN_DGTZ_Success) {
    printf("<v1720CONET2::InitializeForAcq> Error in CAEN_DGTZ_SetDPP_PSD_VirtualProbe %d\n",ret);
    return (int)retval;
  }

  /* WARNING: The mallocs MUST be done after the digitizer programming,
     because the following functions needs to know the digitizer configuration
     to allocate the right memory amount */
  /* Allocate memory for the readout buffer */
  uint32_t AllocatedSize = 0;
  buffer = NULL;
  retval = CAEN_DGTZ_MallocReadoutBuffer(_device_handle, &buffer, &AllocatedSize);
  printf("<v1720CONET2::InitializeForAcq> readout buffer malloced size=%d\n",AllocatedSize);
  if (retval != CAEN_DGTZ_Success) {
    printf("<v1720CONET2::InitializeForAcq> Error in CAEN_DGTZ_MallocReadoutBuffer %d\n",retval);
    return (int)retval;
  }

  /* Allocate memory for the events */
  AllocatedSize = 0;
  retval |= CAEN_DGTZ_MallocDPPEvents(_device_handle, (void**)Events, &AllocatedSize); 
  printf("<v1720CONET2::InitializeForAcq> dpp events malloced size=%d\n",AllocatedSize);
  if (retval != CAEN_DGTZ_Success) {
    printf("<v1720CONET2::InitializeForAcq> Error in CAEN_DGTZ_MallocDPPEvents %d\n",retval);
    return (int)retval;
  }

  /* Allocate memory for the waveforms */
  AllocatedSize = 0;
  retval |= CAEN_DGTZ_MallocDPPWaveforms(_device_handle, (void**)&Waveform, &AllocatedSize); 
  printf("<v1720CONET2::InitializeForAcq> waveforms malloced size=%d\n",AllocatedSize);

  if (retval != CAEN_DGTZ_Success) {
    printf("<v1720CONET2::InitializeForAcq> Error in CAEN_DGTZ_MallocDPPWaveforms %d\n",retval);
    return (int)retval;
  }
  
  _settings_touched = false;

  return 0;
}

//
//-----------------------------------------------------------------------------
/**
 * \brief   Get data type and ZLE configuration
 *
 * Takes the channel configuration (0x8000) as parameter and checks
 * against the fields for data type (pack 2 or pack 2.5) and for ZLE
 * (Zero-length encoding).  Puts the results in fields mDataType and
 * mZLE.
 *
 * \param   [in]  aChannelConfig  Channel configuration (32-bit)
 */
std::string v1720CONET2::GetChannelConfig(DWORD aChannelConfig){
	
  // Set Device, data type and packing for QT calculation later
  int dataType = ((aChannelConfig >> 11) & 0x1);
  if(((aChannelConfig >> 16) & 0xF) == 0) {
    if(dataType == 1) {
      // 2.5 pack, full data
      mDataType = 1;
      mZLE=0;
      return std::string("Raw Data 2.5 Packing");
    } else {
      // 2 pack, full data
      mZLE=0;
      mDataType = 0;
      return std::string("Raw Data");
    }
  } else if(((aChannelConfig >> 16) & 0xF) == 2) {
    if(dataType == 1) {
      // 2.5 pack, ZLE data
      mDataType = 3;
      mZLE=1;
      return std::string("ZLE Data 2.5 Packing");
    } else {
      // 2 pack, ZLE data
      mDataType = 2;
      mZLE=1;
      return std::string("ZLE Data");
    } 
  } else
    return std::string("V1720 Data format Unrecognised");
}

//
//-----------------------------------------------------------------------------
/**
 * \brief   Get ZLE setting
 *
 * Get the current ZLE setting from the channel configuration.
 *
 * \return  true if data is ZLE
 */
BOOL v1720CONET2::IsZLEData(){
  return mZLE;
}

//
//-----------------------------------------------------------------------------
void v1720CONET2::PrintSettings(){
  printf("v1720CONET2::PrintSettings vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv\n");
  printf("setup mode number   = %d\n",config.setup); //!< Initial board setup mode number
  printf("acq_mode            = %d\n",config.acq_mode);       //!< 0x8100@[ 1.. 0]
  printf("channel_config      = %d\n",config.channel_config);          //!< 0x8000@[19.. 0]
  //printf("buffer_organization = %d\n",config.buffer_organization);     //!< 0x800C@[ 3.. 0]
  printf("custom_size         = %d\n",config.custom_size);             //!< 0x8020@[31.. 0]
  printf("channel_mask        = %d\n",config.channel_mask);            //!< 0x8120@[ 7.. 0]
  printf("trigger_source      = %d\n",config.trigger_source);          //!< 0x810C@[31.. 0]
  printf("trigger_output      = %d\n",config.trigger_output);          //!< 0x8110@[31.. 0]
  printf("post_trigger        = %d\n",config.post_trigger);            //!< 0x8114@[31.. 0]
  printf("almost_full         = %d\n",config.almost_full);             //!< 0x816C@[31.. 0]
  // for (int i=0; i<8; i++){
  //   printf("Channel %d Settings:\n",i);
  //   printf("  threshold[%d]:    = %d\n",i,config.threshold[i]);           //!< 0x1n80@[11.. 0]
  //   printf("  nbouthreshold[%d]:= %d\n",i,config.nbouthreshold[i]);       //!< 0x1n%d4@[11.. 0]
  //   printf("  zs_threshold[%d]: = %d\n",i,config.zs_threshold[i]);        //!< 0x1n24@[31.. 0]
  //   printf("  zs_nsamp[%d]:     = %d\n",i,config.zs_nsamp[i]);            //!< 0x1n28@[31.. 0]
  //   printf("  dac[%d]:          = %d\n",i,config.dac[i]);                 //!< 0x1n98@[15.. 0]
  // }
  printf("v1720CONET2::PrintSettings ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n");
  return;
}


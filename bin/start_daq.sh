#!/bin/sh

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" 

./kill_daq.sh

echo Cleaning ODBedit
odbedit -c clean
#odbedit -c "rm /Analyzer/Trigger/Statistics"
#odbedit -c "rm /Analyzer/Scaler/Statistics"
sleep 1

echo Starting mhttpd Midas web server
mhttpd -p 8081 -D

sleep 1
 
echo Starting mlogger Midas Data logger
mlogger -D

sleep 1

echo Please point your web browser to http://localhost:8081
echo Or run: mozilla http://localhost:8081 &

echo Starting Chrome Web server if present
/usr/bin/google-chrome-stable http://localhost:8081 &

# xterm -e ./frontend &
# xterm -e ./analyzer &

#end file
